#!/usr/bin/env python
#

import math
import argparse

def encode_alt_modes(alt, bit13):
    mbit = False
    qbit = True
    encalt = (int(alt) + 1000) / 25

    if bit13 is True:
        tmp1 = (encalt & 0xfe0) << 2
        tmp2 = (encalt & 0x010) << 1

    else:
        tmp1 = (encalt & 0xff8) << 1
        tmp2 = 0

    return (encalt & 0x0F) | tmp1 | tmp2 | (mbit << 6) | (qbit << 4)

latz = 15

def nz(ctype):
	return 4 * latz - ctype

def dlat(ctype, surface):
	if surface == 1:
		tmp = 90.0
	else:
		tmp = 360.0

	nzcalc = nz(ctype)
	if nzcalc == 0:
		return tmp
	else:
		return tmp / nzcalc

def nl(declat_in):
	if abs(declat_in) >= 87.0:
		return 1.0
	return math.floor( (2.0*math.pi) * math.acos(1.0- (1.0-math.cos(math.pi/(2.0*latz))) / math.cos( (math.pi/180.0)*abs(declat_in) )**2 )**-1)

def dlon(declat_in, ctype, surface):
	if surface:
		tmp = 90.0
	else:
		tmp = 360.0
	nlcalc = max(nl(declat_in)-ctype, 1)
	return tmp / nlcalc

#encode CPR position
def cpr_encode(lat, lon, ctype, surface):
    if surface is True:
        scalar = 2.**19
    else:
        scalar = 2.**17

    #encode using 360 constant for segment size.
    dlati = dlat(ctype, False)
    yz = math.floor(scalar * ((lat % dlati)/dlati) + 0.5)
    rlat = dlati * ((yz / scalar) + math.floor(lat / dlati))
    
    #encode using 360 constant for segment size.
    dloni = dlon(lat, ctype, False)
    xz = math.floor(scalar * ((lon % dloni)/dloni) + 0.5)
    
    yz = int(yz) & (2**17-1)
    xz = int(xz) & (2**17-1)
    
    return (yz, xz) #lat, lon

# the polynominal generattor code for CRC
GENERATOR = "1111111111111010000001001"

def hex2bin(hexstr):
    """Convert a hexdecimal string to binary string, with zero fillings. """
    scale = 16
    num_of_bits = len(hexstr) * math.log(scale, 2)
    binstr = bin(int(hexstr, scale))[2:].zfill(int(num_of_bits))
    return binstr

def bin2int(binstr):
    """Convert a binary string to integer. """
    return int(binstr, 2)

def crc(msg, encode=False):
    """Mode-S Cyclic Redundancy Check
    Detect if bit error occurs in the Mode-S message
    Args:
        msg (string): 28 bytes hexadecimal message string
        encode (bool): True to encode the date only and return the checksum
    Returns:
        string: message checksum, or partity bits (encoder)
    """

    msgbin = list(hex2bin(msg))

    if encode:
        msgbin[-24:] = ['0'] * 24

    # loop all bits, except last 24 piraty bits
    for i in range(len(msgbin)-24):
        # if 1, perform modulo 2 multiplication,
        if msgbin[i] == '1':
            for j in range(len(GENERATOR)):
                # modulo 2 multiplication = XOR
                msgbin[i+j] = str((int(msgbin[i+j]) ^ int(GENERATOR[j])))

    # last 24 bits
    reminder = ''.join(msgbin[-24:])
    return reminder
    
###############################################################

def bin2dec(buf):
  if 0 == len(buf): # Crap input
    return -1
  return int(buf, 2)

def get_parity(msg, extended):
  msg_length = len(msg)
  payload = msg[:msg_length - 24]
  parity = msg[msg_length - 24:]

  data = bin2dec(payload[0:32])
  if extended:
    data1 = bin2dec(payload[32:64])
    data2 = bin2dec(payload[64:]) << 8

  hex_id = bin2dec(parity) << 8

  for i in range(0, len(payload)):
    if ((data & 0x80000000) != 0):
      data ^= 0xFFFA0480
    data <<= 1

    if extended:
      if ((data1 & 0x80000000) != 0):
        data |= 1
      data1 <<= 1

      if ((data2 & 0x80000000) != 0):
        data1 = data1 | 1
      data2 <<= 1

  return data
  #return (data ^ hex_id) >> 8

###############################################################

"""
Functions to do (7,4) hamming encoding and decoding, including error detection
and correction.
Manchester encoding and decoding is also included, and by default will use
least bit ordering for the byte that is to be included in the array.
"""

def extract_bit(byte, pos):
    """
    Extract a bit from a given byte using MS ordering.
    ie. B7 B6 B5 B4 B3 B2 B1 B0
    """
    return (byte >> pos) & 0x01

def manchester_encode(byte):
    """
    Encode a byte using Manchester encoding. Returns an array of bits.
    Adds two start bits (1, 1) and one stop bit (0) to the array.
    """
    # Add start bits (encoded 1, 1)
    # manchester_encoded = [0, 1, 0, 1]
    manchester_encoded = []

    # Encode byte
    for i in range(7, -1, -1):
        if extract_bit(byte, i):
            manchester_encoded.append(0)
            manchester_encoded.append(1)
        else:
            manchester_encoded.append(1)
            manchester_encoded.append(0)

    # Add stop bit (encoded 0)
    # manchester_encoded.append(1)
    # manchester_encoded.append(0)

    return manchester_encoded

import numpy
#from scipy.signal import hilbert

def df17_surface_position(_format, ca, icao, tc, mov, s, trk, time, lat, lon, surface):

    #format = 17

    enc_alt =  encode_alt_modes(alt, surface)
    #print "Alt(%r): %X " % (surface, enc_alt)

    #encode that position
    (evenenclat, evenenclon) = cpr_encode(lat, lon, False, surface)
    (oddenclat, oddenclon)   = cpr_encode(lat, lon, True, surface)

    print "Even Lat/Lon: %X/%X " % (evenenclat, evenenclon)
    print "Odd  Lat/Lon: %X/%X " % (oddenclat, oddenclon)

    ff = 0
    df17_even_bytes = []
    df17_even_bytes.append((_format<<3) | ca)
    df17_even_bytes.append((icao>>16) & 0xff)
    df17_even_bytes.append((icao>> 8) & 0xff)
    df17_even_bytes.append((icao    ) & 0xff)
    # data
    df17_even_bytes.append((tc<<3) & 0xf8 | (mov>>4) & 0x07)
    df17_even_bytes.append((mov<<4) & 0xf0 | (s<<3) & 0x08 | (trk>>4) & 0x07)
    df17_even_bytes.append((trk<<4) & 0xf0 | (time<<3) & 0x08 | (ff<<2) & 0x04 | (evenenclat>>15 & 0x03))
    df17_even_bytes.append((evenenclat>>7) & 0xff)
    df17_even_bytes.append(((evenenclat & 0x7f) << 1) | (evenenclon>>16)) 
    df17_even_bytes.append((evenenclon>>8) & 0xff)   
    df17_even_bytes.append((evenenclon   ) & 0xff)

    df17_str = "{0:02x}{1:02x}{2:02x}{3:02x}{4:02x}{5:02x}{6:02x}{7:02x}{8:02x}{9:02x}{10:02x}".format(*df17_even_bytes[0:11])
    print df17_str , "%X" % bin2int(crc(df17_str+"000000", encode=True)) , "%X" % get_parity(hex2bin(df17_str+"000000"), extended=True)
    df17_crc = bin2int(crc(df17_str+"000000", encode=True))

    df17_even_bytes.append((df17_crc>>16) & 0xff)
    df17_even_bytes.append((df17_crc>> 8) & 0xff)
    df17_even_bytes.append((df17_crc    ) & 0xff)
 
    ff = 1
    df17_odd_bytes = []
    df17_odd_bytes.append((_format<<3) | ca)
    df17_odd_bytes.append((icao>>16) & 0xff)
    df17_odd_bytes.append((icao>> 8) & 0xff)
    df17_odd_bytes.append((icao    ) & 0xff)
    # data
    df17_odd_bytes.append((tc<<3) & 0xf8 | (mov>>4) & 0x07)
    df17_odd_bytes.append((mov<<4) & 0xf0 | (s<<3) & 0x08 | (trk>>4) & 0x07)
    df17_odd_bytes.append((trk<<4) & 0xf0 | (time<<3) & 0x08 | (ff<<2) & 0x04 | (oddenclat>>15 & 0x03))
    df17_odd_bytes.append((oddenclat>>7) & 0xff)    
    df17_odd_bytes.append(((oddenclat & 0x7f) << 1) | (oddenclon>>16))  
    df17_odd_bytes.append((oddenclon>>8) & 0xff)   
    df17_odd_bytes.append((oddenclon   ) & 0xff)

    df17_str = "{0:02x}{1:02x}{2:02x}{3:02x}{4:02x}{5:02x}{6:02x}{7:02x}{8:02x}{9:02x}{10:02x}".format(*df17_odd_bytes[0:11])
    print df17_str , "%X" % bin2int(crc(df17_str+"000000", encode=True)) , "%X" % get_parity(hex2bin(df17_str+"000000"), extended=True)
    df17_crc = bin2int(crc(df17_str+"000000", encode=True))

    df17_odd_bytes.append((df17_crc>>16) & 0xff)
    df17_odd_bytes.append((df17_crc>> 8) & 0xff)
    df17_odd_bytes.append((df17_crc    ) & 0xff)    


    for i in range(len(df17_odd_bytes)):
        print (format(df17_odd_bytes[i],'08b'))

    for i in range(len(df17_even_bytes)):
        print (format(df17_even_bytes[i],'08b'))

    
    return (df17_even_bytes, df17_odd_bytes)

def df17_aircraft_id(_format,ca,icao,tc,ec,c1,c2,c3,c4,c5,c6,c7,c8):

    df17_aircraft_id_bytes = []
    df17_aircraft_id_bytes.append((_format<<3) | ca)
    df17_aircraft_id_bytes.append((icao>>16) & 0xff)
    df17_aircraft_id_bytes.append((icao>> 8) & 0xff)
    df17_aircraft_id_bytes.append((icao    ) & 0xff)
    # data
    df17_aircraft_id_bytes.append((tc<<3) | ec)
    df17_aircraft_id_bytes.append((c1<<2) & 0xfc | (c2>>4) & 0x03)
    df17_aircraft_id_bytes.append((c2<<4) & 0xf0 | (c3>>2) & 0x0f)
    df17_aircraft_id_bytes.append((c3<<6) & 0xc0 | (c4) & 0x3f)
    df17_aircraft_id_bytes.append((c5<<2) & 0xfc | (c6>>4) & 0x03)
    df17_aircraft_id_bytes.append((c6<<4) & 0xf0 | (c7>>2) & 0x0f)
    df17_aircraft_id_bytes.append((c7<<6) & 0xc0 | (c8) & 0x3f)

    df17_str = "{0:02x}{1:02x}{2:02x}{3:02x}{4:02x}{5:02x}{6:02x}{7:02x}{8:02x}{9:02x}{10:02x}".format(*df17_aircraft_id_bytes[0:11])
    print df17_str , "%X" % bin2int(crc(df17_str+"000000", encode=True)) , "%X" % get_parity(hex2bin(df17_str+"000000"), extended=True)
    df17_crc = bin2int(crc(df17_str+"000000", encode=True))

    df17_aircraft_id_bytes.append((df17_crc>>16) & 0xff)
    df17_aircraft_id_bytes.append((df17_crc>> 8) & 0xff)
    df17_aircraft_id_bytes.append((df17_crc    ) & 0xff)

    for i in range(len(df17_aircraft_id_bytes)):
        print (format(df17_aircraft_id_bytes[i],'08b'))

    return df17_aircraft_id_bytes


def df17_pos_rep_encode(ca, icao, tc, ss, nicsb, alt, time, lat, lon, surface):

    format = 17

    enc_alt =	encode_alt_modes(alt, surface)
    #print "Alt(%r): %X " % (surface, enc_alt)

    #encode that position
    (evenenclat, evenenclon) = cpr_encode(lat, lon, False, surface)
    (oddenclat, oddenclon)   = cpr_encode(lat, lon, True, surface)

    #print "Even Lat/Lon: %X/%X " % (evenenclat, evenenclon)
    #print "Odd  Lat/Lon: %X/%X " % (oddenclat, oddenclon)

    ff = 0
    df17_even_bytes = []
    df17_even_bytes.append((format<<3) | ca)
    df17_even_bytes.append((icao>>16) & 0xff)
    df17_even_bytes.append((icao>> 8) & 0xff)
    df17_even_bytes.append((icao    ) & 0xff)
    # data
    df17_even_bytes.append((tc<<3) | (ss<<1) | nicsb)
    df17_even_bytes.append((enc_alt>>4) & 0xff)
    df17_even_bytes.append((enc_alt & 0xf) << 4 | (time<<3) | (ff<<2) | (evenenclat>>15))
    df17_even_bytes.append((evenenclat>>7) & 0xff)    
    df17_even_bytes.append(((evenenclat & 0x7f) << 1) | (evenenclon>>16))  
    df17_even_bytes.append((evenenclon>>8) & 0xff)   
    df17_even_bytes.append((evenenclon   ) & 0xff)

    df17_str = "{0:02x}{1:02x}{2:02x}{3:02x}{4:02x}{5:02x}{6:02x}{7:02x}{8:02x}{9:02x}{10:02x}".format(*df17_even_bytes[0:11])
    print df17_str , "%X" % bin2int(crc(df17_str+"000000", encode=True)) , "%X" % get_parity(hex2bin(df17_str+"000000"), extended=True)
    df17_crc = bin2int(crc(df17_str+"000000", encode=True))

    df17_even_bytes.append((df17_crc>>16) & 0xff)
    df17_even_bytes.append((df17_crc>> 8) & 0xff)
    df17_even_bytes.append((df17_crc    ) & 0xff)
 
    ff = 1
    df17_odd_bytes = []
    df17_odd_bytes.append((format<<3) | ca)
    df17_odd_bytes.append((icao>>16) & 0xff)
    df17_odd_bytes.append((icao>> 8) & 0xff)
    df17_odd_bytes.append((icao    ) & 0xff)
    # data
    df17_odd_bytes.append((tc<<3) | (ss<<1) | nicsb)
    df17_odd_bytes.append((enc_alt>>4) & 0xff)
    df17_odd_bytes.append((enc_alt & 0xf) << 4 | (time<<3) | (ff<<2) | (oddenclat>>15))
    df17_odd_bytes.append((oddenclat>>7) & 0xff)    
    df17_odd_bytes.append(((oddenclat & 0x7f) << 1) | (oddenclon>>16))  
    df17_odd_bytes.append((oddenclon>>8) & 0xff)   
    df17_odd_bytes.append((oddenclon   ) & 0xff)

    df17_str = "{0:02x}{1:02x}{2:02x}{3:02x}{4:02x}{5:02x}{6:02x}{7:02x}{8:02x}{9:02x}{10:02x}".format(*df17_odd_bytes[0:11])
    df17_crc = bin2int(crc(df17_str+"000000", encode=True))

    df17_odd_bytes.append((df17_crc>>16) & 0xff)
    df17_odd_bytes.append((df17_crc>> 8) & 0xff)
    df17_odd_bytes.append((df17_crc    ) & 0xff)    


    for i in range(len(df17_odd_bytes)):
        print (bin(df17_odd_bytes[i]))

    
    return (df17_even_bytes, df17_odd_bytes)


def df17_airborne_velocities(_format, ca, icao, tc, st, ic, resv_a, nac, param1, param2, param3, param4, vr_src, s_vr, vr, resv_b, s_dif,  dif):
   df17_airborne_velocities_bytes = []
   df17_airborne_velocities_bytes.append((_format<<3) | ca)
   df17_airborne_velocities_bytes.append((icao>>16) & 0xff)
   df17_airborne_velocities_bytes.append((icao>> 8) & 0xff)
   df17_airborne_velocities_bytes.append((icao    ) & 0xff)
   # data
   df17_airborne_velocities_bytes.append((tc<<3) | (st))
   df17_airborne_velocities_bytes.append((ic<<7) & 0x80 | (resv_a<<6) & 0x40 | (nac<<3) & 0x38 | (param1<<2) & 0x04 | (param2>>8) & 0x03)
   df17_airborne_velocities_bytes.append((param2) & 0xff)
   df17_airborne_velocities_bytes.append((param3<<7) & 0x80 | (param4>>3) & 0x7f)    
   df17_airborne_velocities_bytes.append(((param4<<5) & 0xe0 | (vr_src<<4) & 0x10 | (s_vr<<3) & 0x08 | (vr>>6) & 0x07))  
   df17_airborne_velocities_bytes.append((vr<<2) & 0xfc | (resv_b) & 0x03)   
   df17_airborne_velocities_bytes.append((s_dif<<7) & 0x80 | (dif) & 0x7f)

   df17_str = "{0:02x}{1:02x}{2:02x}{3:02x}{4:02x}{5:02x}{6:02x}{7:02x}{8:02x}{9:02x}{10:02x}".format(*df17_airborne_velocities_bytes[0:11])
   print df17_str , "%X" % bin2int(crc(df17_str+"000000", encode=True)) , "%X" % get_parity(hex2bin(df17_str+"000000"), extended=True)
   df17_crc = bin2int(crc(df17_str+"000000", encode=True))

   for i in range(len(df17_airborne_velocities_bytes)):
        print (format(df17_airborne_velocities_bytes[i],'08b'))

   df17_airborne_velocities_bytes.append((df17_crc>>16) & 0xff)
   df17_airborne_velocities_bytes.append((df17_crc>> 8) & 0xff)
   df17_airborne_velocities_bytes.append((df17_crc    ) & 0xff)

 


   return df17_airborne_velocities_bytes


def frame_1090es_ppm_modulate(even, odd):
    ppm = [ ]

    for i in range(48):    # pause
        ppm.append( 0 )

    ppm.append( 0xA1 )   # preamble
    ppm.append( 0x40 )
    
    for i in range(len(even)):
        word16 = numpy.packbits(manchester_encode(~even[i]))
        ppm.append(word16[0])
        ppm.append(word16[1])


    for i in range(100):    # pause
        ppm.append( 0 )

    ppm.append( 0xA1 )   # preamble
    ppm.append( 0x40 )

    for i in range(len(odd)):
        word16 = numpy.packbits(manchester_encode(~odd[i]))
        ppm.append(word16[0])
        ppm.append(word16[1])

    for i in range(48):    # pause
        ppm.append( 0 )

    #print '[{}]'.format(', '.join(hex(x) for x in ppm))
    
    return bytearray(ppm)

def hackrf_raw_IQ_format(ppm):
    """
    real_signal = []
    bits = numpy.unpackbits(numpy.asarray(ppm, dtype=numpy.uint8))
    for bit in bits:
        if bit == 1:
            I = 127
        else:
            I = 0
        real_signal.append(I)

    analytic_signal = hilbert(real_signal)

    #for i in range(len(real_signal)):
    #    print i, real_signal[i], int(analytic_signal[i])
    """

    signal = []
    bits = numpy.unpackbits(numpy.asarray(ppm, dtype=numpy.uint8))
    for bit in bits:
        if bit == 1:
            I = 127
            Q = 127
        else:
            I = 0
            Q = 0
        signal.append(I)
        signal.append(Q)

    return bytearray(signal)

def get_first_menu():

    def print_menu():       # Your menu design here
        print(30 * "-" + "ADS-B Fuzzer" + 30 * "-")
        print("1. Aircraft Identification")
        print("2. Surface Position")
        print("3. Airbirne Position")
        print("4. Airborne Velocities")        
        print("5. Exit")
        print(73 * "-")

    loop = True
    int_choice = -1

    while loop:          # While loop that keeps going until loop = False
        print_menu()    # Displays menu
        choice = raw_input("Enter your choice [1-5]: ")
        print choice
        #Aircraft Identification
        if choice == '1':
            int_choice = 1
            values = []
            charset = "?ABCDEFGHIJKLMNOPQRSTUVWXYZ????? ???????????????0123456789??????"
            print "For each of the following fields please choose a fixed value or type 'random' to use random values for this field."
            while len(choice) == 0:
                choice = raw_input("TC : ")
                values.append(choice)
            while len(choice) == 0:
                choice = ''
                choice = raw_input("EC : ")
            while len(choice) == 0:
                choice = ''
                choice = raw_input("Character 1 : ")
            while len(choice) == 0:
                choice = ''
                choice = raw_input("Character 2 : ")
            while len(choice) == 0:
                choice = ''
                choice = raw_input("Character 3 : ")
            while len(choice) == 0:
                choice = ''
                choice = raw_input("Character 4 : ")
            while len(choice) == 0:
                choice = ''
                choice = raw_input("Character 5 : ")
            while len(choice) == 0:
                choice = ''
                choice = raw_input("Character 6 : ")
            while len(choice) == 0:
                choice = ''
                choice = raw_input("Character 7 : ")
            while len(choice) == 0:
                choice = ''
                choice = raw_input("Character 8 : ")                


            loop = False
        elif choice == '2':
            choice = ''
            while len(choice) == 0:
                choice = raw_input("Enter custom folder name(s). It may be a list of folder's names (example: c:,d:\docs): ")
            int_choice = 2
            loop = False
        elif choice == '3':
            choice = ''
            while len(choice) == 0:
                choice = raw_input("Enter a single filename of a file with custom folders list: ")
            int_choice = 3
            loop = False
        elif choice == '4':
            choice = ''
            while len(choice) == 0:
                choice = raw_input("Enter a single filename of a conf file: ")
            int_choice = 4
            loop = False
        elif choice == '5':
            int_choice = -1
            print("Exiting..")
            loop = False  # This will make the while loop to end
        else:
            # Any inputs other than values 1-5 we print an error message
            raw_input("Wrong menu selection. Enter any key to try again..")
    return [int_choice, choice]

    
if __name__ == "__main__":


    parser = argparse.ArgumentParser(description='Welcome to the ADS-B Fuzzer!',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("-t", "--type", help="specify the type of the message", choices=("acid","sp","ap","av"))
    parser.add_argument("-tx","--transmit",help="if specified and available will transmit with the plugged in HackRF One",action="store_true")
    parser.add_argument("-o", "--output", "--outputfile", help="Output file to write generated data")
    parser.add_argument("-icao", "--icao", help="specify the ICAO address of the aircraft in hexadecimal (6 characters long)",default="random")
    parser.add_argument("-pi", "--parityinformation", help="specify the partity information in hexadecimal (6 characters long)",default="automatic")
    parser.add_argument("-df", "--dataformat", help="specify the data format (DF) of the message",default=17)
    parser.add_argument("-hex", "--hexadecimal", help="every input must be written in hexadecimal format",action="store_true")
    parser.add_argument("-ca","--capability", help="capability field",choices=range(-1,8),type=int,default='random')
    args = parser.parse_args()
    if args.tc:
      print "typecode turned on"
    print args.dataformat
    print args.hexadecimal
    print args.capability






    #from sys import argv, exit
    
    #argc = len(argv)
    #if argc != 5:
    #  print
    #  print 'Usage: '+ argv[0] +'  <ICAO> <Latitude> <Longitude> <Altitude>'
    #  print
    #  print '    Example: '+ argv[0] +'  0xABCDEF 12.34 56.78 9999.0'
    #  print
    #  exit(2)

    #icao = int(argv[1], 16)
    #lat = float(argv[2])
    #lon = float(argv[3])
    #alt = float(argv[4])

    _format = 17
    ca = 5
    tc = 4
    ec = 0
    c1 = 11
    c2 = 12
    c3 = 13
    c4 = 49
    c5 = 48
    c6 = 50
    c7 = 51
    c8 = 32
    

    #surface position
    #tc = 7
    mov = 42#40
    s = 1
    trk = 50#35
    time = 0
    surface = True



    tc = 19
    st = 1
    ic = 0
    resv_a = 1
    nac = 0
    param1 = 1
    param2 = 9
    param3 = 1
    param4 = 160
    vr_src = 0
    s_vr = 1
    vr = 14
    resv_b = 0
    s_dif = 0
    dif = 23



#airborne position

    #ca = 5
    #tc = 11
    #ss = 0
    #nicsb = 0    
    #time = 0       
    #surface = False

    #(df17_even, df17_odd) = df17_pos_rep_encode(ca, icao, tc, ss, nicsb, alt, time, lat, lon, surface)
    #df17_array = frame_1090es_ppm_modulate(df17_even, df17_odd)
    #df17_aircraft_id_complete = df17_aircraft_id(_format, ca, icao, tc, ec, c1, c2, c3, c4, c5, c6, c7, c8)
    #(df17_even, df17_odd) = df17_surface_position(_format, ca, icao, tc, mov, s, trk, time, lat, lon, surface)
    #df17_airborne_velocities_complete = df17_airborne_velocities(_format, ca, icao, tc, st, ic, resv_a, nac, param1, param2, param3, param4, vr_src, s_vr, vr, resv_b, s_dif, dif)
    #df17_array = frame_preamble(df17_aircraft_id_complete)
    #df17_array = frame_1090es_ppm_modulate(df17_aircraft_id_complete, df17_aircraft_id_complete)
    #df17_array = frame_1090es_ppm_modulate(df17_even, df17_odd)
    #df17_array = frame_1090es_ppm_modulate(df17_airborne_velocities_complete,df17_airborne_velocities_complete)
    #samples_array = hackrf_raw_IQ_format(df17_array)
    #SamplesFile = open("Samples.iq8s", "wb")
    #SamplesFile.write(samples_array)
    #SamplesFile.write(samples_array)
    #SamplesFile.write(samples_array)
    #print(get_first_menu())






#aircrfat id


    



    
    #print ''.join(format(x, '02x') for x in df17_even)
    #print ''.join(format(x, '02x') for x in df17_odd)
    
    

    
    
    
    

    #OutFile = open("filename.bin", "wb")
    #OutFile.write(df17_array)

    

###############################################################
